package cv.ptsiber.wbdb;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class WbdbApplication {

    public static void main(String[] args) {
        SpringApplication.run(WbdbApplication.class, args);
    }
}

package cv.ptsiber.wbdb.features.connection;

import org.springframework.data.repository.CrudRepository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

public interface ConnectionRepository extends CrudRepository<Connection, UUID> {

    Optional<Connection> findByIdAndOwner(UUID id, String owner);

    @Transactional(timeout = 30)
    List<Connection> findByOwner(String owner);

}

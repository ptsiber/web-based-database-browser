package cv.ptsiber.wbdb.features.introspect;

import com.zaxxer.hikari.HikariDataSource;
import cv.ptsiber.wbdb.features.connection.Connection;
import cv.ptsiber.wbdb.features.connection.ConnectionService;
import cv.ptsiber.wbdb.features.introspect.data.Column;
import cv.ptsiber.wbdb.features.introspect.data.PrimaryKey;
import cv.ptsiber.wbdb.features.introspect.data.Schema;
import cv.ptsiber.wbdb.features.introspect.data.Table;
import lombok.Setter;
import lombok.SneakyThrows;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jdbc.repository.config.DialectResolver;
import org.springframework.data.relational.core.dialect.Dialect;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.PathVariable;

import java.sql.DatabaseMetaData;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.MessageFormat;
import java.util.*;
import java.util.stream.Collectors;

@Service
public class IntrospectionService {

    @Setter(onMethod = @__(@Autowired))
    private ConnectionService connectionService;

    @SneakyThrows
    public List<Schema> listSchemas(@PathVariable("id") UUID id, String username) {
        Connection jdbcConnection = connectionService.getConnection(id, username);

        HikariDataSource dataSource = new HikariDataSource();
        dataSource.setJdbcUrl(jdbcConnection.getJdbcUrl());
        dataSource.setUsername(jdbcConnection.getUsername());
        dataSource.setPassword(jdbcConnection.getPassword());

        BeanPropertyRowMapper<Schema> schemaMapper = BeanPropertyRowMapper.newInstance(Schema.class);

        try (var connection = dataSource.getConnection()) {
            DatabaseMetaData metaData = connection.getMetaData();
            ResultSet schemasRS = metaData.getSchemas();

            return resultSetToList(schemaMapper, schemasRS);
        }
    }

    @SneakyThrows
    public List<Table> listTables(UUID id, String schema, String username) {
        Connection jdbcConnection = connectionService.getConnection(id, username);

        HikariDataSource dataSource = new HikariDataSource();
        dataSource.setJdbcUrl(jdbcConnection.getJdbcUrl());
        dataSource.setUsername(jdbcConnection.getUsername());
        dataSource.setPassword(jdbcConnection.getPassword());

        BeanPropertyRowMapper<Table> tableMapper = BeanPropertyRowMapper.newInstance(Table.class);

        try (var connection = dataSource.getConnection()) {
            DatabaseMetaData metaData = connection.getMetaData();
            ResultSet tablesRS = metaData.getTables(null, schema, null, null);

            List<Table> tables = resultSetToList(tableMapper, tablesRS);

            for (Table table : tables) {
                table.setPrimaryKeys(getTablePrimaryKeys(metaData, table.getTableName()));
            }

            return tables;
        }
    }

    @SneakyThrows
    private List<Table.PrimaryKey> getTablePrimaryKeys(DatabaseMetaData metaData, String tableName) {
        ResultSet resultSet = metaData.getPrimaryKeys(null, null, tableName);

        Map<String, List<PrimaryKey>> primaryKeysJdbc = resultSetToList(BeanPropertyRowMapper.newInstance(PrimaryKey.class), resultSet).stream()
                .collect(Collectors.groupingBy(PrimaryKey::getPkName));

        List<Table.PrimaryKey> primaryKeys = new ArrayList<>();
        for (Map.Entry<String, List<PrimaryKey>> primaryKeyJdbc : primaryKeysJdbc.entrySet()) {
            Table.PrimaryKey primaryKey = new Table.PrimaryKey(primaryKeyJdbc.getKey());
            primaryKeyJdbc.getValue().stream()
                    .sorted(Comparator.comparing(PrimaryKey::getKeySeq))
                    .forEach(pkColumn -> primaryKey.getPrimaryKeyColumnNames().add(pkColumn.getColumnName()));

            primaryKeys.add(primaryKey);
        }

        return primaryKeys;
    }

    @SneakyThrows
    public List<Column> listColumns(UUID id, String schema, String table, String username) {
        Connection jdbcConnection = connectionService.getConnection(id, username);

        HikariDataSource dataSource = new HikariDataSource();
        dataSource.setJdbcUrl(jdbcConnection.getJdbcUrl());
        dataSource.setUsername(jdbcConnection.getUsername());
        dataSource.setPassword(jdbcConnection.getPassword());

        BeanPropertyRowMapper<Column> columnMapper = BeanPropertyRowMapper.newInstance(Column.class);

        try (var connection = dataSource.getConnection()) {
            DatabaseMetaData metaData = connection.getMetaData();
            ResultSet columnsRS = metaData.getColumns(null, schema, table, null);

            return resultSetToList(columnMapper, columnsRS);
        }
    }

    @SneakyThrows
    public List<Map<String, Object>> previewData(UUID id, String schema, String table, String username) {
        Connection jdbcConnection = connectionService.getConnection(id, username);

        HikariDataSource dataSource = new HikariDataSource();
        dataSource.setJdbcUrl(jdbcConnection.getJdbcUrl());
        dataSource.setUsername(jdbcConnection.getUsername());
        dataSource.setPassword(jdbcConnection.getPassword());


        JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);
        Dialect dialect = DialectResolver.getDialect(jdbcTemplate);

        return jdbcTemplate.queryForList(MessageFormat.format("SELECT * FROM {0}.{1} {2}",
                schema, table, dialect.limit().getLimit(25)));
    }

    private <T> List<T> resultSetToList(RowMapper<T> rowMapper, ResultSet resultSet) throws SQLException {
        int rowNum = 0;
        List<T> tables = new ArrayList<>();
        while (resultSet.next()) {
            T record = rowMapper.mapRow(resultSet, rowNum);
            tables.add(record);
        }

        return tables;
    }
}

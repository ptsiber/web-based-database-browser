package cv.ptsiber.wbdb.features.introspect.data;

import lombok.Data;

@Data
public class Schema {
    private String tableSchem;
    private String tableCatalog;
}

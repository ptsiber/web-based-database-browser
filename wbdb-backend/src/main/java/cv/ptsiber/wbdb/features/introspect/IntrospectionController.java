package cv.ptsiber.wbdb.features.introspect;

import cv.ptsiber.wbdb.features.introspect.data.Column;
import cv.ptsiber.wbdb.features.introspect.data.Schema;
import cv.ptsiber.wbdb.features.introspect.data.Table;
import lombok.Setter;
import lombok.SneakyThrows;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.security.Principal;
import java.util.List;
import java.util.Map;
import java.util.UUID;

@RestController
@RequestMapping("/wbdb-api/introspection")
public class IntrospectionController {

    @Setter(onMethod = @__(@Autowired))
    private IntrospectionService introspectionService;

    @GetMapping("/{id}/list-schemas")
    public List<Schema> listSchemas(@PathVariable("id") UUID id, Principal principal) {
        return introspectionService.listSchemas(id, principal.getName());
    }

    @GetMapping("/{id}/{schema}/list-tables")
    public List<Table> listTables(@PathVariable("id") UUID id, @PathVariable("schema") String schema, Principal principal) {
        return introspectionService.listTables(id, schema, principal.getName());
    }

    @GetMapping("/{id}/{schema}/{table}/list-columns")
    public List<Column> listColumns(@PathVariable("id") UUID id, @PathVariable("schema") String schema, @PathVariable("table") String table, Principal principal) {
        return introspectionService.listColumns(id, schema, table, principal.getName());
    }

    @GetMapping("/{id}/{schema}/{table}/preview")
    public List<Map<String, Object>> previewData(@PathVariable("id") UUID id, @PathVariable("schema") String schema, @PathVariable("table") String table, Principal principal) {
        return introspectionService.previewData(id, schema, table, principal.getName());
    }

}

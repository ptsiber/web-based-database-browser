package cv.ptsiber.wbdb.features.introspect.data;

import lombok.Data;

@Data
public class Column {
    private String tableCat;
    private String tableSchem;
    private String tableName;
    private String columnName;
    // see java.sql.Types
    private Integer dataType;
    private String typeName;
    private Integer columnSize;
    private Integer decimalDigits;
    private Integer numPrecRadix;
    private Integer nullable;
    private String remarks;
    private String columnDef;
    private Integer charOctetLength;
    private Integer ordinalPosition;
    private String isNullable;
    private String isAutoincrement;
    private String isGeneratedColumn;
}

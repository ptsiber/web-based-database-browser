package cv.ptsiber.wbdb.features.introspect.data;

import lombok.Data;
import lombok.RequiredArgsConstructor;

import java.util.ArrayList;
import java.util.List;

@Data
public class Table {
    private String tableCat;
    private String tableSchem;
    private String tableName;
    private String tableType;
    private String remarks;
    private String typeCat;
    private String typeSchem;
    private String typeName;
    private String selfReferencingColName;
    private String refGeneration;

    private List<PrimaryKey> primaryKeys = new ArrayList<>();

    @Data
    public static class PrimaryKey {
        private final String primaryKeyName;
        private List<String> primaryKeyColumnNames = new ArrayList<>();
    }
}

package cv.ptsiber.wbdb.features.connection;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.CreatedBy;
import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.PersistenceConstructor;

import javax.validation.constraints.NotEmpty;
import java.util.UUID;

@Data
@NoArgsConstructor
@AllArgsConstructor(access = AccessLevel.PRIVATE, onConstructor = @__(@PersistenceConstructor))
public class Connection {
    @Id
    private UUID id;
    private String displayName;
    @NotEmpty(message = "jdbcUrl can not be empty")
    private String jdbcUrl;
    @NotEmpty(message = "username can not be empty")
    private String username;
    private String password;
    private String connectionDetailsJson;
    @CreatedBy
    private String owner;
}

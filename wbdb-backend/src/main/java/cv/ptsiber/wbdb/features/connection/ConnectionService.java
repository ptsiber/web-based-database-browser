package cv.ptsiber.wbdb.features.connection;

import com.google.common.base.Preconditions;
import lombok.Setter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.DeleteMapping;

import java.util.List;
import java.util.UUID;

@Service
public class ConnectionService {

    @Setter(onMethod = @__(@Autowired))
    private ConnectionRepository connectionRepository;

    public Connection insertConnection(Connection connection) {
        Preconditions.checkArgument(connection.getId() == null, "Connection was persisted before");

        return connectionRepository.save(connection);
    }

    public Connection updateConnection(Connection connection, String username) {
        Preconditions.checkArgument(connection.getId() != null, "Can't update connection. Id is missing");
        Preconditions.checkArgument(connection.getOwner().equals(username), "Can't update connection you do not own");

        return connectionRepository.save(connection);
    }

    public Connection getConnection(UUID id, String username) {
        return connectionRepository.findByIdAndOwner(id, username)
                .orElseThrow(() -> new IllegalStateException("Can't find connection. Probably it was deleted or you are not an owner"));
    }

    public List<Connection> listConnections(String username) {
        return connectionRepository.findByOwner(username);
    }

    @DeleteMapping
    public void deleteConnection(UUID id) {
        connectionRepository.deleteById(id);
    }

}

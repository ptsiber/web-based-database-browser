package cv.ptsiber.wbdb.features.connection;

import lombok.Setter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.security.Principal;
import java.util.List;
import java.util.UUID;

@RestController
@RequestMapping("/wbdb-api/connection")
public class ConnectionController {

    @Setter(onMethod = @__(@Autowired))
    private ConnectionService connectionService;

    @PostMapping
    public Connection insertConnection(@Valid Connection connection) {
        return connectionService.insertConnection(connection);
    }

    @PutMapping
    public Connection updateConnection(@Valid Connection connection, Principal principal) {
        return connectionService.updateConnection(connection, principal.getName());
    }

    @GetMapping("/{id}")
    public Connection getConnection(@PathVariable("id") UUID id, Principal principal) {
        return connectionService.getConnection(id, principal.getName());
    }

    @GetMapping("/list")
    public List<Connection> listConnections(Principal principal) {
        return connectionService.listConnections(principal.getName());
    }

    @DeleteMapping
    public void deleteConnection(UUID id) {
        connectionService.deleteConnection(id);
    }

}

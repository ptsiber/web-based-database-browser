package cv.ptsiber.wbdb.features.introspect.data;

import lombok.Data;

@Data
public class PrimaryKey {
    private String columnName;
    private Short keySeq;
    private String pkName;
}

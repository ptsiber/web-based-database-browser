package cv.ptsiber.wbdb.security;

import org.springframework.http.HttpStatus;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.password.NoOpPasswordEncoder;
import org.springframework.security.web.csrf.CookieCsrfTokenRepository;

import javax.servlet.http.Cookie;

@EnableGlobalMethodSecurity(securedEnabled = true)
public class SecurityConfiguration extends WebSecurityConfigurerAdapter {

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        // @formatter:off
        http
/*
                .httpBasic()
                .exceptionHandling()
                    .defaultAuthenticationEntryPointFor(new HttpStatusEntryPoint(HttpStatus.UNAUTHORIZED), new AntPathRequestMatcher("/wbdb-api/**"))
            .and()
*/
                .formLogin()
                    .loginProcessingUrl("/authentication/login")
                    .failureHandler((request, response, exception)-> response.sendError(HttpStatus.UNAUTHORIZED.value(), exception.getMessage()))
            .and()
                .logout()
                    .logoutUrl("/authentication/logout")
                    .logoutSuccessHandler((request, response, authentication) -> {
                        response.addCookie(new Cookie("XSRF-TOKEN", CookieCsrfTokenRepository.withHttpOnlyFalse().generateToken(request).getToken()));
                        response.setStatus(HttpStatus.OK.value());
                    })
            .and()
                .authorizeRequests()
                    .antMatchers("/authentication/**").permitAll()
                    .antMatchers("/wbdb-api/**").authenticated()
                    .antMatchers("/wbdb-public-api/**").permitAll()
                    .anyRequest().permitAll()
            .and()
                .csrf()
                .   csrfTokenRepository(CookieCsrfTokenRepository.withHttpOnlyFalse());
        // @formatter:on
    }

    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        // @formatter:off
        auth
            .inMemoryAuthentication()
                .passwordEncoder(NoOpPasswordEncoder.getInstance())
                .withUser("junior")
                    .password("junior")
                    .roles("READ")
            .and()
                .withUser("senior")
                    .password("senior")
                    .roles("READ", "WRITE");
        // @formatter:on
    }

}

### Please start the application WbdbApplication.
You can add JDBC driver in pom.xml for any of following DB
- MySQL
- H2
- DB2
- HSQL
- Postgres
- SqlServer
- Oracle

There is a preconfigured in-memory DB H2

### Here is the list of links you can try immediately
- http://localhost:8080/wbdb-api/connection/list
- http://localhost:8080/wbdb-api/introspection/dac11ffe-163c-4b92-a953-99084259d74f/list-schemas
- http://localhost:8080/wbdb-api/introspection/dac11ffe-163c-4b92-a953-99084259d74f/PUBLIC/list-tables
- http://localhost:8080/wbdb-api/introspection/dac11ffe-163c-4b92-a953-99084259d74f/PUBLIC/CONNECTION/list-columns
- http://localhost:8080/wbdb-api/introspection/dac11ffe-163c-4b92-a953-99084259d74f/PUBLIC/CONNECTION/preview